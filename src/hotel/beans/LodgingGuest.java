package hotel.beans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


// Проживание гостей в номерах отеля (РАССЕЛЕНИЕ) т.е. учетная запись гостя в гостинице
public final class LodgingGuest {

    private final Guest guest;
    private final Room room;
    private final Date arrivalDate;   // дата заселения
    private Date leavingDate;   // дата выселения (может меняться, т.к. гостя могут выселить досрочно)
    private final List<ProvidedService> providedServices;   // список использованных услуг


    // *** constrictor ***
    public LodgingGuest(Guest guest, Room room, Date arrivalDate, Date leavingDate, List<ProvidedService> providedServices) {
        this.guest = guest;
        this.room = room;
        this.arrivalDate = arrivalDate;
        this.leavingDate = leavingDate;
        this.providedServices = providedServices;
    }
    public LodgingGuest(Guest guest, Room room, Date arrivalDate, Date leavingDate) {
        this(guest, room, arrivalDate, leavingDate, new ArrayList<>());   // у только что прибывшего гостя сформирован новый чистый лист оказанных услуг
    }


    // *** set / get ***
    public Guest getGuest() {
        return guest;
    }
    public Room getRoom() {
        return room;
    }
    public Date getArrivalDate() {
        return arrivalDate;
    }
    public Date getLeavingDate() {
        return leavingDate;
    }
    public void setLeavingDate(Date leavingDate) {
        this.leavingDate = leavingDate;
    }
    public List<ProvidedService> getProvidedServices() {
        return providedServices;
    }


    // *** metods ***
    // добавим ОКАЗАННУЮ УСЛУГУ к существующему списку (или добавим список/ массив услуг)
    public void addService(ProvidedService providedService) {
        this.providedServices.add(providedService);
    }
    public void addService(ProvidedService... providedServices) {
        for (ProvidedService service: providedServices) {
            addService(service);
        }
    }

    // в настоящий момент жилец еще проживает в отеле или уже выписан из номера.
    public boolean isAvailableNow() {
        long now = System.currentTimeMillis();
        return (now > arrivalDate.getTime() && now < leavingDate.getTime())   // если текущая дата в диапазоне между прибытием и отбытием то true
                ? true
                : false;
    }
    // на указанную дату жилец проживает в номере?
    public boolean isAvailableToDate(Date date) {
        long pointTime = date.getTime();
        return (pointTime > arrivalDate.getTime() && pointTime < leavingDate.getTime())   // если текущая дата в диапазоне между прибытием и отбытием то true
                ? true
                : false;
    }
    // на указанный диапазон дат жилец вообще прибывал ли в отеле?
    public boolean isAvailableToIntervalDate(Date testArrivalDate, Date testLeavingDate) {
        long testArrivalTime = testArrivalDate.getTime();
        long testLeavingTime = testLeavingDate.getTime();
        boolean result = false;
        if (testArrivalTime < testLeavingTime) {   // если введена ВЕРНО, т.е. введена дата прибытия раньше дата отбытия позже
            if ((testArrivalTime >= this.arrivalDate.getTime()   &&   testArrivalTime <= this.leavingDate.getTime()) ||
                    (testLeavingTime >= this.arrivalDate.getTime()   &&   testLeavingTime <= this.leavingDate.getTime())) {
                result = true;
            }
        };
        return result;
    }

    // сумма к оплате (номер + услуги)
    public int totalPaymentForRoom() {
        int resultSum = 0;
        for (ProvidedService providedService: providedServices) {   // переберем оказанные услуги
            resultSum += providedService.getPriceService();   // сумируем цены за них
        }
        resultSum += this.room.getCost();   // добавим стоимость самого номера.
        return resultSum;
    }


    // *** @Override ***
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LodgingGuest that = (LodgingGuest) o;
        return Objects.equals(guest, that.guest) &&
                Objects.equals(room, that.room) &&
                Objects.equals(arrivalDate, that.arrivalDate) &&
                Objects.equals(leavingDate, that.leavingDate) &&
                Objects.equals(providedServices, that.providedServices);
    }

    @Override
    public int hashCode() {

        return Objects.hash(guest, room, arrivalDate, leavingDate, providedServices);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(" LodgingGuests:\n");
        result.append("guest_name = " + guest.getName());
        result.append(",   room_№ = " + room.getNumber());
        result.append(",   arrivalDate = " + arrivalDate);
        result.append(",   leavingDate = " + leavingDate  + '\n');
        result.append(" Services:\n");
        for (ProvidedService providedService: providedServices) {
            result.append("\t" + providedService + "\n");
        }
        result.append("total payment for room = " + totalPaymentForRoom());
        return  result.toString();
    }

}