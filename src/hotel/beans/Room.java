package hotel.beans;

import hotel.enums.*;

import java.util.Objects;


public class Room {

    private final int number;   // №
    private int cost;   // цена
    private final NumberBeds numberBeds;   // кол-во мест
    private final StarsRoom stars;   // кол-во звезд
    private StatusRoom statusRoom;   // текущий статус номера


    //   primary constructor
    public Room(int number, int cost, NumberBeds numberBeds, StarsRoom stars, StatusRoom statusRoom) {
        this.number = number;
        this.cost = cost;
        this.numberBeds = numberBeds;
        this.stars = stars;
        this.statusRoom = statusRoom;
    }

    public Room(int number, int cost, NumberBeds numberBeds, StarsRoom stars) {
        this(number, cost, numberBeds, stars, StatusRoom.FREE);
    }

    public Room(int number, NumberBeds numberBeds, StarsRoom stars) {
        this(number, numberBeds.getPrice(), numberBeds, stars, StatusRoom.FREE);
    }


    public int getNumber() {
        return number;
    }

    public int getCost() {
        return cost;
    }
    public void setCost(int cost) {
        this.cost = cost;
    }

    public NumberBeds getNumberBeds() {
        return numberBeds;
    }

    public StarsRoom getStars() {
        return stars;
    }

    public StatusRoom getStatusRoom() {
        return statusRoom;
    }
    public void setStatusRoom(StatusRoom statusRoom) {
        this.statusRoom = statusRoom;
    }


    // *** @Override ***

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return number == room.number &&
                numberBeds == room.numberBeds &&
                stars == room.stars;
    }

    @Override
    public int hashCode() {

        return Objects.hash(number, numberBeds, stars);
    }

    @Override
    public String toString() {
        return "Room{" +
                " number = " + number +
                ",   cost = " + cost +
                ",   numberBeds = " + numberBeds +
                ",   stars = " + stars +
                ",   statusRoom = " + statusRoom +
                " }";
    }

}
