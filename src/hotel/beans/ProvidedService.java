package hotel.beans;

import hotel.enums.ServicePrice;
import java.sql.Date;
import java.util.Objects;


// Оказываемая услуга   - актуальные данные.
public final class ProvidedService {

    private final String nameService;
    private int priceService;
    private final Date date;


    // *** constructor ***
    public ProvidedService(String nameService, int priceService, Date date) {
        this.nameService = nameService;
        this.priceService = priceService;
        this.date = date;
    }
    public ProvidedService(ServicePrice service, Date date) {
        this(service.getName(), service.getPrice(), date);
    }
    public ProvidedService(ServicePrice service, int priceService, Date date) {
        this(service.getName(), priceService, date);
    }


    // *** Get / set ***
    public String getNameService() {
        return nameService;
    }
    public int getPriceService() {
        return priceService;
    }
    public Date getDate() {
        return date;
    }


    // *** @Override ***

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProvidedService service = (ProvidedService) o;
        return Objects.equals(nameService, service.nameService);
    }

    @Override
    public int hashCode() {

        return Objects.hash(nameService);
    }

    @Override
    public String toString() {
        return
                "\t nameService = " + nameService +
                ",   \t priceService=" + priceService +
                ",   \t date=" + date;
    }
}
