package hotel.beans;

import hotel.helpers.Builder;

import java.sql.Date;
import java.util.List;
import java.util.Objects;


public final class Guest {

    private final String name;   // имя постояльца
    private final Date birthday;


    // primary constructor
    public Guest(String name, Date birthday) {
        this.name = name;
        this.birthday = birthday;
    }

    public Guest(String name, List<ProvidedService> services) {
        this(name, Builder.createBirthdayDateGuest());
    }


    // ***   get / set   ***
    public String getName() {
        return name;
    }

    public Date getBirthday() {
        return birthday;
    }



    // *** @Override ***

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Guest guest = (Guest) o;
        return (name.equals(guest.name)) &&
                (birthday.equals(guest.birthday));
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, birthday);
    }

    @Override
    public String toString() {
        return "Guest{" +
                " name = '" + name + '\'' +
                ", birthday = " + birthday +
                " }";
    }

}
