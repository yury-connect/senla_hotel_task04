package hotel.helpers;

import hotel.beans.*;
import hotel.enums.*;

import java.sql.Date;
import java.util.*;


public final class Builder {
    private static final Random RANDOM = new Random();
    private static int guestCounter;



    // Create RoomList ****************************************************************************************************
    public static List<Room> buildRoomList(int sizeRoomList, Map<NumberBeds, Integer> roomPrice) {   // № апортаментов, Количество мест размещения
        List<Room> rooms = new ArrayList<>(sizeRoomList);
        for (int i = 1; i <= sizeRoomList; i++) {
            Room room = createRoom(i, roomPrice);
            rooms.add(room);
        }
        return rooms;
    }


    private static Room createRoom(int numberRoom, Map<NumberBeds, Integer> roomPrice) {
        int variantRoom = roomPrice.size();

        Object[] numberBedsObj = roomPrice.keySet().toArray();
        NumberBeds[] numberBeds = new NumberBeds[variantRoom];
        for (int i = 0; i < variantRoom; i++) {
            numberBeds[i] = (NumberBeds) numberBedsObj[i];
        }

        int n = RANDOM.nextInt(variantRoom);
        NumberBeds numberBedsRoom = numberBeds[n];   // сколькиместный номер

        StarsRoom[] stars = StarsRoom.values();
        StarsRoom starsRoom = stars[RANDOM.nextInt(stars.length)];   // сколькизвездный номер

        int costRoom = roomPrice.get(numberBedsRoom);

        Room room = new Room(numberRoom, costRoom, numberBedsRoom, starsRoom);
        return room;
    }



    // Create GuestList ****************************************************************************************************
    public static List<Guest> buildGuestList(int sizeGuestList) {
        List<Guest> guests = new ArrayList<>(sizeGuestList);
        for (int i = 1; i <= sizeGuestList; i++) {
            String nameGuest = "Guest_№ " + guestCounter++;
            Date birthdayGuest = new Date(System.currentTimeMillis() - (18 + RANDOM.nextInt(72)) * 1000 * 60 * 60 * 24 * 30 * 12);
            Guest guest = new Guest(nameGuest, birthdayGuest);
            guests.add(guest);
        }
        return guests;
    }



    // lane рождения 'birthday' для класса 'Guest'
    public static Date createBirthdayDateGuest() {
        long timeMilisec = System.currentTimeMillis() - ((1000 * 60 * 60 * 24 * 30 * 12) * (18 + new Random().nextInt(72)));
        return new Date(timeMilisec);
    }


    // *** создать базовый (начальный) прайс-лист на НОМЕРА ОТЕЛЯ используя цены по умолчанию из enum NumberBeds ***
    public static Map<NumberBeds, Integer> createRoomPrice() {
        Map<NumberBeds, Integer> roomPrice = new HashMap<>();
        for (NumberBeds bed: NumberBeds.values()) {
            roomPrice.put(bed, bed.getPrice());
        }
        return roomPrice;
    }

    // *** создать базовый (начальный) прайс-лист на УСЛУГИ ОТЕЛЯ используя цены по умолчанию из enum Service ***
    public static Map<String, Integer> createServicePrice() {
        Map<String, Integer> servicePrice = new HashMap<>();   // параметризируем Map<String, Integer> а не Map<ServicePrice, Integer> т.к. предусматриваем возможность добавления услуг в будующем.
        for (ServicePrice service: ServicePrice.values()) {
            servicePrice.put(service.getName(), service.getPrice());
        }
        return servicePrice;
    }

}
