package hotel.comparators.guestComparators;

import hotel.beans.Guest;

public class GuestNameComparator implements GuestComparator {

    @Override
    public int compare(Guest guest1, Guest guest2) {
        return guest1.getName().compareTo(guest2.getName());
    }

}
