package hotel.comparators.guestComparators;

import hotel.beans.Guest;

public class GuestBirthdayComparator implements GuestComparator {

    @Override
    public int compare(Guest guest1, Guest guest2) {
        return guest1.getBirthday().compareTo(guest2.getBirthday());
    }

}
