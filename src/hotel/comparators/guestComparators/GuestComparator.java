package hotel.comparators.guestComparators;

import hotel.beans.Guest;

import java.util.Comparator;

public interface GuestComparator extends Comparator<Guest> {
}
