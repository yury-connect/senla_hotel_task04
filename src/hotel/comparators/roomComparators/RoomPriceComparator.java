package hotel.comparators.roomComparators;

import hotel.beans.Room;

public class RoomPriceComparator implements RoomComparator {


    @Override
    public int compare(Room room1, Room room2) {
        return room1.getCost() - room2.getCost();
    }

}
