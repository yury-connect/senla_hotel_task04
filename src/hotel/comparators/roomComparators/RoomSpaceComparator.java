package hotel.comparators.roomComparators;

import hotel.beans.Room;

public class RoomSpaceComparator implements RoomComparator {


    @Override
    public int compare(Room room1, Room room2) {
        return room1.getNumberBeds().getNumberSpacesInRoom() - room2.getNumberBeds().getNumberSpacesInRoom();
    }

}
