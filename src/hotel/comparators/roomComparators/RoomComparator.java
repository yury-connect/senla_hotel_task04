package hotel.comparators.roomComparators;

import hotel.beans.Room;

import java.util.Comparator;

public interface RoomComparator extends Comparator<Room> {
}
