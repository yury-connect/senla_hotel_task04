package hotel.comparators.roomComparators;

import hotel.beans.Room;

public class RoomStarsComparator implements RoomComparator {


    @Override
    public int compare(Room room1, Room room2) {
        return room1.getStars().getNumberStars() - room2.getStars().getNumberStars();
    }

}
