package hotel.comparators.lodgingGuestComparators;

import hotel.beans.LodgingGuest;

public class LodgingGuestNameComparator implements LodgingGuestComparator {

    @Override
    public int compare(LodgingGuest lodgingGuests1, LodgingGuest lodgingGuests2) {
        return lodgingGuests1.getGuest().getName().compareTo(lodgingGuests2.getGuest().getName());
    }

}
