package hotel.comparators.lodgingGuestComparators;

import hotel.beans.LodgingGuest;

public class LodgingGuestLeavingDateComparator implements LodgingGuestComparator {

    @Override
    public int compare(LodgingGuest lodgingGuests1, LodgingGuest lodgingGuests2) {
        return lodgingGuests1.getLeavingDate().compareTo(lodgingGuests2.getLeavingDate());
    }

}
