package hotel.comparators.lodgingGuestComparators;

import hotel.beans.LodgingGuest;

import java.util.Comparator;

public interface LodgingGuestComparator extends Comparator<LodgingGuest> {
}
