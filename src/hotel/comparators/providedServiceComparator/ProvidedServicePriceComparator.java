package hotel.comparators.providedServiceComparator;

import hotel.beans.ProvidedService;

public class ProvidedServicePriceComparator implements ProvidedServiceComparator{

    @Override
    public int compare(ProvidedService providedService1, ProvidedService providedService2) {
        return providedService1.getPriceService() - providedService2.getPriceService();
    }

}
