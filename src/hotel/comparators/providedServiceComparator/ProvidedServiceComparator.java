package hotel.comparators.providedServiceComparator;

import hotel.beans.ProvidedService;

import java.util.Comparator;

public interface ProvidedServiceComparator extends Comparator<ProvidedService> {
}
