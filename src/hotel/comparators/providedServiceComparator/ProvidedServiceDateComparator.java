package hotel.comparators.providedServiceComparator;

import hotel.beans.ProvidedService;

public class ProvidedServiceDateComparator implements ProvidedServiceComparator {

    @Override
    public int compare(ProvidedService providedService1, ProvidedService providedService2) {
        return providedService1.getDate().compareTo(providedService2.getDate());
    }

}
