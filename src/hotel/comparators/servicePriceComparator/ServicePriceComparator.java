package hotel.comparators.servicePriceComparator;

import hotel.enums.ServicePrice;

import java.util.Comparator;
import java.util.Map;

public interface ServicePriceComparator extends Comparator<Map.Entry<String, Integer>> {
}
