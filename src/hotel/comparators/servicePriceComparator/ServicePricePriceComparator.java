package hotel.comparators.servicePriceComparator;

import java.util.Map;

public class ServicePricePriceComparator  implements ServicePriceComparator {

    @Override
    public int compare(Map.Entry<String, Integer> entry1, Map.Entry<String, Integer> entry2) {
        return entry1.getValue() - entry2.getValue();
    }

}