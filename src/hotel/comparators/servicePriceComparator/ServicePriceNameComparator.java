package hotel.comparators.servicePriceComparator;

import java.util.Map;


public class ServicePriceNameComparator  implements ServicePriceComparator {

    @Override
    public int compare(Map.Entry<String, Integer> entry1, Map.Entry<String, Integer> entry2) {
        return entry1.getKey().compareTo(entry2.getKey());
    }

}