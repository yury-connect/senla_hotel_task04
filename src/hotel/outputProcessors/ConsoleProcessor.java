package hotel.outputProcessors;


public class ConsoleProcessor {

    public void sendMssage(String message) {
        System.out.println(message);
    }

    public void sendMssage(String[] messages) {
        for (String message: messages) {
            sendMssage(message);
        }
    }

}
