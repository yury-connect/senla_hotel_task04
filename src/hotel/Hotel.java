package hotel;

import hotel.beans.*;
import hotel.enums.*;
import hotel.helpers.Builder;

import java.sql.Date;
import java.util.*;


public class Hotel {

    private int totalNumberRooms;   // сколько всего сумарно мест в гостинице
    private final Map<NumberBeds, Integer> roomPrice;   // прайс-лист отеля на комнаты, MAP
    private final List<Room> roomList;   // List комнат отеля
    private final Map<String, Integer> servicePrice;   // параметризируем Map<String, Integer> а не Map<ServicePrice, Integer> т.к. предусматриваем возможность добавления услуг в будующем.
    private final List<LodgingGuest> lodgingGuestsList;   // учетки всех гостей отеля
    private static final Random RANDOM = new Random();


// *** constructor ***
    public Hotel(int totalNumberRooms) {
        this.totalNumberRooms = totalNumberRooms;
        this.roomPrice = Builder.createRoomPrice();   // заполним значениями по умолчанию
        this.servicePrice = Builder.createServicePrice();   // заполним значениями по умолчанию
        this.roomList = Builder.buildRoomList(totalNumberRooms, roomPrice);   // создадим комнаты
        this.lodgingGuestsList = new ArrayList<>();   // при создании отеля в нем чисты список заселений гостей
    }


// *** get / set ***
    public int getTotalNumberRooms() {
        return totalNumberRooms;
    }
    public Map<NumberBeds, Integer> getRoomPrice() {
        return roomPrice;
    }
    public List<Room> getRoomList() {
        return roomList;
    }
    public List<Room> getRoomListByStatus(StatusRoom statusRoom) {
        List<Room> result = new ArrayList<>();
        for (Room room: roomList) {
            if (statusRoom == room.getStatusRoom()) {
                result.add(room);
            }
        }
        return result;
    }
    public Map<String, Integer> getServicePrice() {
        return servicePrice;
    }
    public List<Guest> getGuestList() {
        List<Guest> guestList = new ArrayList<>();
        for (LodgingGuest lodgingGuest: lodgingGuestsList) {
            guestList.add(lodgingGuest.getGuest());
        }
        return guestList;
    }
    public List<LodgingGuest> getLodgingGuestsList() {
        return lodgingGuestsList;
    }


// *** servisec ***

    // получить список посетителей данного номера
    public List<LodgingGuest> getLodgingGuestsListByRoom(Room room) {
        List<LodgingGuest> result = new ArrayList<>();
        for (LodgingGuest lodgingGuest: lodgingGuestsList) {
            if (room.equals(lodgingGuest.getRoom())) {
                result.add(lodgingGuest);
            }
        }
        return result;
    }

    // поселение переданного гостя на указанное кол-во дней в отель (создание учетки)
    public boolean putGuestToHotel(Guest guests, int durationStayDays) {   // Гость, Продолжительность пребывания в днях
        boolean isSuccess;   // пока гость не будет заселен в номер, статус заселения будет  false
        Room roomForGuest = getAvailableRoomByRandom();   // получим номер со свободным койка-местом
//        Room roomForGuest = getFreeRoomByRandom();   // получим свободный номер
        isSuccess = putGuestToHotel(guests, roomForGuest, durationStayDays);
        return isSuccess;   // возвращаем статус заселение гостя.
    }

    // поселение переданного гостя в указанную комнату на указанное кол-во дней в отель (создание учетки)
    public boolean putGuestToHotel(Guest guests, Room roomForGuest, int durationStayDays) {   // Гость, Комната, Продолжительность пребывания в днях
        boolean isSuccess;   // пока гость не будет заселен в номер, статус заселения будет  false
        if (roomForGuest.getStatusRoom() == StatusRoom.FREE || roomForGuest.getStatusRoom() == StatusRoom.PART_TIME) {   // в комнате есть свободное место
            Date arrivalDate = new Date(System.currentTimeMillis());   // дата заселения
            Date leavingDate = new Date(arrivalDate.getTime() + durationStayDays * 24*60*60*1000);   // дата выселения

            final int numberServicesMin = 1;   // МИНИМАЛЬНОЕ количество оказанных постояльцу услуг
            final int numberServicesMax = 10;   // МАКСИМАЛЬНОЕ количество оказанных постояльцу услуг
            final int numberServices = numberServicesMin + RANDOM.nextInt(numberServicesMax - numberServicesMin);   // определяем количество услг
            List<ProvidedService> providedServices = new ArrayList<>(numberServices);

            List<Map.Entry<String, Integer>> servicePriceList = new ArrayList<>(servicePrice.entrySet());   // конвертировали прайс в List
            for (int i = 0; i < numberServices; i++) {
                Map.Entry<String, Integer> entry = servicePriceList.get(RANDOM.nextInt(servicePriceList.size()));   // получим случайную услугу
                String nameService = entry.getKey();
                int priceService = entry.getValue();
                Date dateService = new Date(   // случайная дата между прибытием и отбытием
                        arrivalDate.getTime() + RANDOM.nextInt(durationStayDays) * 24*60*60*1000
                );
                ProvidedService newProvidedService = new ProvidedService(nameService, priceService, dateService);   // создадим очередную оказанную услугу
                providedServices.add(newProvidedService);   // добавим созданную услугу в список услуг
            }
            LodgingGuest newLodgingGuests = new LodgingGuest(guests, roomForGuest, arrivalDate, leavingDate, providedServices);   // создадим новую учетку
            if ( (getNumGuestInRoom(roomForGuest, arrivalDate) + 1)   // Если в комнате поживает на 1 жилец меньше,
                    >= roomForGuest.getNumberBeds().getNumberSpacesInRoom()) {   // чем количество коек-мест в ней (т.е. комната после поселения в нее жильца будет заполнена полностью)
                roomForGuest.setStatusRoom(StatusRoom.BUSY);   // то меняем статус комнаты на "заполнена"   StatusRoom.BUSY
            } else {   // в противном случае получается, что в комнате еще есть свободные места
                roomForGuest.setStatusRoom(StatusRoom.PART_TIME);   // т.о. меняем статус на "частично занята"
            }
            this.lodgingGuestsList.add(newLodgingGuests);   // добавляем новую учетку
            isSuccess = true;   // статус заселения - УСПЕХ
        } else {
            isSuccess = false;   // статус заселения - НЕ УДАЛОСЬ
        }
        return isSuccess;   // заселение гостя прошло удасно.
    }

    // удаляем переданного гостя из номера отеля (удаляем его учетку полностью)
    public boolean removeGuestFromHotel(Guest guests) {
        boolean isSuccess = false;
        for (int i = 0; i < lodgingGuestsList.size(); i++) {
            if (lodgingGuestsList.get(i).getGuest().equals(guests)) {
                lodgingGuestsList.remove(i);
                isSuccess = true;
            }
        }
        return isSuccess;
    }


        // предоставит СЛУЧАЙНО ВЫБРАННУЮ комнату со СВОБОДНОЙ КОЙКА-МЕСТОМ (лотерея удачи для гостя)
    public Room getAvailableRoomByRandom() throws NullPointerException{
        List<Room> availableRooms = getAvailableRoomsNow();
        int size = availableRooms.size();
        Room room;
        if (size == 0) {
            room = null; // ошибка, нет свободных номеров
        } else {
            room = availableRooms.get(RANDOM.nextInt(size));
        }
        return room;
    }
    // предоставит СЛУЧАЙНО ВЫБРАННУЮ комнату со СВОБОДНУЮ ПОЛНОСТЬЮ (без посетителей)
    private Room getFreeRoomByRandom() throws NullPointerException{
        List<Room> freeRooms = getFreeRoomsNow();
        int size = freeRooms.size();
        Room room;
        if (size == 0) {
            room = null; // ошибка, нет свободных номеров
        } else {
            room = freeRooms.get(RANDOM.nextInt(size));
        }
        return room;
    }
    // предоставит список СЛУЧАЙНО ВЫБРАННЫХ комнат со СВОБОДНОЙ КОЙКА-МЕСТОМ
    public List<Room> getAvailableRoomsNow() {
        StatusRoom[] statusRooms = {StatusRoom.FREE, StatusRoom.PART_TIME};
        return getRoomListByStatusRoomNow(statusRooms);
    }
    // предоставит список СЛУЧАЙНО ВЫБРАННУЮ комнат полностью СВОБОДНЫХ (без посетителей)
    public List<Room> getFreeRoomsNow() {
        StatusRoom[] statusRooms = {StatusRoom.FREE};
        return getRoomListByStatusRoomNow(statusRooms);
    }

    // получить количество Гостей, проживающих в этой комнате на ЗАДАННУЮ дату
    private int getNumGuestInRoom(Room room, Date date) {
        int result = 0;
        long testTime = date.getTime();
        for (LodgingGuest lodgingGuests: lodgingGuestsList) { // переберем все учетки гостей
            if (lodgingGuests.getRoom().equals(room)   &&
                    testTime   >=   lodgingGuests.getArrivalDate().getTime()   &&
                    testTime   <=   lodgingGuests.getLeavingDate().getTime()) {
                result ++;
            }
        }
        return result;
    }

    // получить список комнат, у которых статус соответствует одному из указанных
    private List<Room> getRoomListByStatusRoomNow(StatusRoom... statusRooms) {
        List<Room> availableRooms = new ArrayList<>();
        for (Room room: roomList) {   // перебор всех номеров гостиницы
            for (StatusRoom currentStatusRoom: statusRooms) {   // перебор переданных статусов (как массив)
                if (room.getStatusRoom() == currentStatusRoom) {
                    availableRooms.add(room);
                }
            }
        }
        return availableRooms;
    }

    // получить список комнат, которые будут свободны к определенной дате в будущем
    public List<Room> getFreeRoomListByDate(Date date) {
        List<Room> result = new ArrayList<>(roomList);
        for (LodgingGuest lodgingGuests: lodgingGuestsList) { // переберем все учетки гостей
            if (lodgingGuests.getLeavingDate().getTime() >= date.getTime()   // если номер к заданной дате будет занят
                    &&   result.contains(lodgingGuests.getRoom())) { // и он еще присутствует в списке СВОБОДНЫХ номеров,
                result.remove(lodgingGuests.getRoom());   // то удалим его из списка.
            }
        }
        return result;
    }

    // проживал ли ЗАДАННЫЙ постоялец в ЗАДАННОЙ комнате в ЗАДАННЫЙ интервал дат (в любую из дат)
    private boolean isGuestLivedInRoomIntervalDateAsOr(Guest guest, Room room, Date testArrivalDate, Date testLeavingDate) {
        boolean result = false;
        long testArrivalTime = testArrivalDate.getTime();
        long testLeavingTime = testLeavingDate.getTime();
        if (testArrivalTime < testLeavingTime) {   // если введена ВЕРНО, т.е. введена дата прибытия раньше дата отбытия позже
            for (LodgingGuest lodgingGuests: lodgingGuestsList) {
                if (lodgingGuests.getGuest().equals(guest)   &&   lodgingGuests.getRoom().equals(room)) { // если постоялец вообще проживал когда - либо в этом номере
                    if (    (testArrivalTime >= lodgingGuests.getArrivalDate().getTime()
                            &&   testArrivalTime <= lodgingGuests.getLeavingDate().getTime())   ||
                                    (testLeavingTime >= lodgingGuests.getArrivalDate().getTime()
                                    &&   testLeavingTime <= lodgingGuests.getLeavingDate().getTime())   ) {
                        result = true;   // если постоялец проживал в номере, то проверим интервал дат.
                        break; // Если попали в интервал, то   true   и выход из цикла.
                    }
                }
            }
        }
        return  result;
    }

    // список проживающих НА ДАННЫЙ МОМЕНТ жильцов гостиницы
    private List<Guest> getGuestListAvailableNow() {
        List<Guest> guestList = new ArrayList<>();
        for (LodgingGuest lodgingGuests: lodgingGuestsList) {
            if (lodgingGuests.isAvailableNow()) {
                guestList.add(lodgingGuests.getGuest());
            }
        }
        return guestList;
    }

    // список проживающих НА УКАЗАННУЮ ДАТУ жильцов гостиницы
    private List<Guest> getGuestListAvailableToDate(Date date) {
        List<Guest> guestList = new ArrayList<>();
        for (LodgingGuest lodgingGuests: lodgingGuestsList) {
            if (lodgingGuests.isAvailableToDate(date)) {
                guestList.add(lodgingGuests.getGuest());
            }
        }
        return guestList;
    }

    // список проживающих НА УКАЗАННЫЙ ДИАПАЗОН ДАТ жильцов гостиницы
    private List<Guest> getGuestListAvailableToIntervalDate(Date testArrivalDate, Date testLeavingDate) {
        List<Guest> guestList = new ArrayList<>();
        for (LodgingGuest lodgingGuests: lodgingGuestsList) {
            if (lodgingGuests.isAvailableToIntervalDate(testArrivalDate, testLeavingDate)) {
                guestList.add(lodgingGuests.getGuest());
            }
        }
        return guestList;
    }

    // пересчитать суммарное количество номеров в отеле С УКАЗАННЫМ СТАТУСОМ
    private int calculateTotalRoomsByStatusRoom(StatusRoom statusRoom) {
        int result = 0;
        for (Room room: roomList) {
            if (room.getStatusRoom().equals(statusRoom)) {
                result++;
            }
        }
        return result;
    }

    // пересчитать суммарное количество номеров в отеле С УКАЗАННЫМ КОЛИЧЕСТВОМ ЗВЕЗД
    private int calculateTotalRoomsByStars(StarsRoom stars) {
        int result = 0;
        for (Room room: roomList) {
            if (room.getStars().equals(stars)) {
                result++;
            }
        }
        return result;
    }

}
