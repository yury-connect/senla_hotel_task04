package hotel.enums;


// перечень статусов состояния номера
public enum  StatusRoom {

    FREE,   //свободен
    BUSY,   // занят
    PART_TIME,   // частично занят
    REPAIR;   // на ремонте (не обслуживаем)

    public String getName() {
        return this.name().toLowerCase();
    }

}
