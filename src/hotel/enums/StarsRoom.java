package hotel.enums;



public enum StarsRoom {

    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5);



    int numberStars;

    StarsRoom(int numberStars) {
        this.numberStars = numberStars;
    }


    public int getNumberStars() {
        return numberStars;
    }
    public String getName() {
        return this.name().toLowerCase();
    }

}
