package hotel.enums;


// перечень доступных услуг
public enum ServicePrice {

    BREAKFAST(100),   // услуга ЗАВТРАК стоит 100
    BEACH_TOWEL(150),   // услуга ПЛЯЖНОЕ ПОЛОТЕНЦЕ стоит 150
    DECKCHAIR(200),   // услуга ШЕЗЛОНГ стоит 200
    GYM(250),   // услуга СПОРТ_ЗАЛ стоит 250
    MASSAGE(300);   // услуга МАСАЖ стоит 300



    private int defaultPrice;

    ServicePrice(int price) {
        this.defaultPrice = price;
    }



    public int getPrice() {
        return defaultPrice;
    }

    public String getName() {
        return this.name().toLowerCase();
    }

}
