package hotel.enums;

// номера бывают следующих видов:
public enum NumberBeds {


    SINGLE(1, 100),   // одноместный номер стоит 50
    DOUBLE(2, 80),   // двухместный номер стоит 60
    QUADRUPLE(4, 60),   // четырехместный номер стоит 80
    MULTI_SEAT(6, 35);   // многоместный номер стоит 90



    private int numberSpacesInRoom;   // количество спальных мест в номере
    private int defaultPrice;   // базовая цена за 1 спальное место в данном номере


    NumberBeds(int numberSpacesInRoom, int price) {
        this.numberSpacesInRoom = numberSpacesInRoom;
        this.defaultPrice = price;
    }


    // наименование МЕСТ-ности номера "по прайсу отеля"
    public String getName() {
        return this.name().toLowerCase();
    }

    // количество спальных мест в номере
    public int getNumberSpacesInRoom() {
        return numberSpacesInRoom;
    }

    // базовая цена за 1 спальное место в данном номере
    public int getPrice() {
        return this.defaultPrice;
    }

}
