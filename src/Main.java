import hotel.Hotel;
import hotel.beans.*;
import hotel.comparators.lodgingGuestComparators.*;
import hotel.comparators.providedServiceComparator.*;
import hotel.comparators.roomComparators.*;
import hotel.comparators.servicePriceComparator.*;
import hotel.enums.NumberBeds;
import hotel.enums.StarsRoom;
import hotel.enums.StatusRoom;
import hotel.helpers.Builder;

import java.sql.Date;
import java.util.*;


/**
 * @author   Lapickiy Yury
 */
public class Main {
    private static final Random RANDOM = new Random();
    private static final String DELIMITER =
            "====================================================================================================";

    public static void main(String[] args) {

        Hotel hotel = new Hotel(10);  // 10 комнат

        List<Guest> guestList = Builder.buildGuestList(15);   // создадим список гостей
        for (int i = 0; i < guestList.size(); i++) {   // заселим гостей в отель
            hotel.putGuestToHotel(guestList.get(i), (i + 1) * 2);   // 1-й гость селится на 2 дня, 2-й на 4 и т.д.
        }


        // 1. получаем список номеров и сортируем;
        {
            List<Room> roomList = hotel.getRoomList();
            roomList.sort(new RoomPriceComparator());
            roomListPrinter("1.1 получаем список номеров сортированный по цене", roomList);
            roomList.sort(new RoomSpaceComparator());
            roomListPrinter("1.2 получаем список номеров сортированный по вместимости", roomList);
            roomList.sort(new RoomStarsComparator());
            roomListPrinter("1.3 получаем список номеров сортированный по кол-ву звезд", roomList);
        }


        // 2. получаем список СВОБОДНЫХ номеров и сортируем;
        {
            List<Room> roomFreeList = hotel.getRoomListByStatus(StatusRoom.FREE);
            roomFreeList.sort(new RoomPriceComparator());
            roomListPrinter("2.1 получаем список СВОБОДНЫХ номеров сортированный по цене", roomFreeList);
            roomFreeList.sort(new RoomSpaceComparator());
            roomListPrinter("2.2 получаем список СВОБОДНЫХ номеров сортированный по вместимости", roomFreeList);
            roomFreeList.sort(new RoomStarsComparator());
            roomListPrinter("2.3 получаем список СВОБОДНЫХ номеров сортированный по кол-ву звезд", roomFreeList);
        }


        //3. Список постояльцев и их номеров;
        {
            List<LodgingGuest> lodgingGuestsList2 = hotel.getLodgingGuestsList();
            lodgingGuestsList2.sort(new LodgingGuestNameComparator());
            lodgingGuestsListPrinter(
                    "3.1 Список постояльцев и из номеров, отсортированный по алфавиту", lodgingGuestsList2);
            lodgingGuestsList2.sort(new LodgingGuestLeavingDateComparator());
            lodgingGuestsListPrinter(
                    "3.2 Список постояльцев и из номеров, отсортированный по дате отправления", lodgingGuestsList2);
        }


        // 4. Общее число своболных комнат;
        {
            int totalNumFreeRooms = hotel.getRoomListByStatus(StatusRoom.FREE).size();
            System.out.println(DELIMITER + "\n4. Общее число свободных комнат = " + totalNumFreeRooms + "\n");
        }


        // 5. Общее число постояльцев;
        {
            int totalNumGuests = hotel.getGuestList().size();
            System.out.println(DELIMITER + "\n5. Общее число постояльцев = " + totalNumGuests + "\n");
        }


        // 6. Список номеров, которые будут свободны по определенной дате в будующем;
        {
            java.sql.Date date = new java.sql.Date(System.currentTimeMillis() + 1 * 24 * 60 * 60 * 1000);   // на завтра
            List<Room> freeRoomListByDate = hotel.getFreeRoomListByDate(date);
            {
                String message = "6. Список номеров, которые будут свободны по определенной дате в будующем: " + date;
                StringBuilder result = new StringBuilder(DELIMITER + "\n" + message + "\n");
                for (Room room: freeRoomListByDate) {
                    result.append(room + "\n");
                }
                result.append("\n");
                System.out.println(result);
            }
        }


        // 7. Суммы оплаты за номер, которую должен оплатить постоялец;
        {
            String message = "7. Суммы оплаты за номер, которую должен оплатить постоялец: ";
            StringBuilder result = new StringBuilder(DELIMITER + "\n" + message + "\n");
            for (LodgingGuest lodgingGuest: hotel.getLodgingGuestsList()) {
                result.append("name of Guest: " + lodgingGuest.getGuest().getName()
                        + " ; \t total payment for room: " + lodgingGuest.totalPaymentForRoom() + " ;\n");
            }
            System.out.println(result);
        }


        // 8. Посмотреть 3 последних постояльцев номера и даты их пребывания;
        {
            Room room = hotel.getRoomList().get(RANDOM.nextInt(hotel.getTotalNumberRooms()));   // выберем КОМНАТУ случайным образом
            String message = "8. Посмотреть 3 последних постояльцев номера  ' " + room.getNumber() + " '  и даты их пребывания";
            StringBuilder result = new StringBuilder(DELIMITER + "\n" + message + "\n");

            List<LodgingGuest> guestsRoomList =  hotel.getLodgingGuestsListByRoom(room);
            guestsRoomList.sort(new LodgingGuestLeavingDateComparator());   // отсортируем по датам отбытия

            result.append("Выведем список всех постояльцев номера  ' " + room.getNumber() + " '  и даты их пребывания:\n");
            for (LodgingGuest lodgingGuest: guestsRoomList) {
                result.append(" постоялец: " + lodgingGuest.getGuest().getName() + " \t ");
                result.append(" дата прибытия: " + lodgingGuest.getArrivalDate() + " \t ");
                result.append(" дата отъезда: " + lodgingGuest.getLeavingDate() + " \n");
            }

            result.append("\nВыведем список 3 последних постояльцев номера  ' " + room.getNumber() + " '  и даты их пребывания:\n");
            int startIndex = guestsRoomList.size() - 1;
            int endIndex = startIndex - 3;
            for (int i = startIndex; i > endIndex && i >= 0; i--) {
                LodgingGuest lodgingGuest = guestsRoomList.get(i);
                result.append(" постоялец: " + lodgingGuest.getGuest().getName() + " \t ");
                result.append(" дата прибытия: " + lodgingGuest.getArrivalDate() + " \t ");
                result.append(" дата отъезда: " + lodgingGuest.getLeavingDate() + " \n");
            }
            System.out.println(result);
        }


        // 9. посмотреть списки услуг постояльца и их цены (сортировать по цене, по дате);
        {
            ProvidedServiceComparator providedServiceComparator;
            StringBuilder result = new StringBuilder(DELIMITER + "\n9.1 список услуг постояльца и их цены, сортируем по цене:\n");
            providedServiceComparator = new ProvidedServicePriceComparator();   // создадим соответствующий компаратор по цене
            result.append(lodgingGuestsListToStringBuilder(hotel.getLodgingGuestsList(), providedServiceComparator) + "\n");

            result.append(DELIMITER + "\n9.2 список услуг постояльца и их цены, сортируем по дате:\n");
            providedServiceComparator = new ProvidedServiceDateComparator();   // создадим соответствующий компаратор по дате
            result.append(lodgingGuestsListToStringBuilder(hotel.getLodgingGuestsList(), providedServiceComparator));
            System.out.println(result);
        }


        // 10. цены услуг и номеров (сортировать по разделу, цене);
        {
            StringBuilder result = new StringBuilder(DELIMITER + "\n10.1 цены услуг, сортируем по наименованию услуги:\n");
            Map<String, Integer> servicePriceMap = hotel.getServicePrice();   // получим прайс отеля
            List<Map.Entry<String, Integer>> servicePriceList = new ArrayList<>(servicePriceMap.entrySet());   // конвертировали в List
            servicePriceList.sort(new ServicePriceNameComparator());   // отсортировали прайс по НАИМЕНОВАНИЮ услуги
            result.append(servicePriceListToStringBuilder(servicePriceList) + "\n");
            result.append(DELIMITER + "\n10.2 цены услуг, сортируем по цене услуги:\n");
            servicePriceList.sort(new ServicePricePriceComparator());   // отсортировали прайс по ЦЕНЕ услуги
            result.append(servicePriceListToStringBuilder(servicePriceList));
            System.out.println(result);
        }


        // 11. посмотреть детали отдельного номера;
        {
            int indexRoom = RANDOM.nextInt(hotel.getTotalNumberRooms());  // выберем случанй номер
            Room room = hotel.getRoomList().get(indexRoom);
            String message = DELIMITER + "\nПросмотрим детали номера с id = " + indexRoom + " :\n";
            System.out.println(message + room);
        }


        // +1. поселить в номер.   (реализовано ранее):
        {
            Guest guest = new Guest("Mary", new Date(1989-06-24));   // создадим нового гостя
            int durationStayDays = 3;   // будем селит его в отель на столько дней
            Room room = hotel.getAvailableRoomByRandom();   // получим произвольную комнату со свободной койка-местом
            if (room == null) {
//                System.out.println("В отеле нет свободных комнат. Заселение невозможно!");
            } else {
                hotel.putGuestToHotel(guest, room, durationStayDays);
//                System.out.println("Гость ' " + guest.getName() + " ' заселен в комнату ' " + room.getNumber() + " '");
            }
        }


        // +2. Выселить из номера.   (реализовано ранее):
        {
            Guest guest = hotel.getGuestList().get(RANDOM.nextInt(hotel.getGuestList().size()));   // получим жильца, которого будем выселять
            boolean isSuccess = hotel.removeGuestFromHotel(guest);
            if (isSuccess) {
//                System.out.println("Гость ' " + guest.getName() + " ' выселен из отеля.");
            } else {
//                System.out.println("Выселение гостя ' " + guest.getName() + " ' из отеля завершилось неудачей.");
            }
        }


        // +3. Изменить статус номера на Ремонтируемый/ обслуживаемый.   (реализовано ранее):
        {
            List<Room> roomList = hotel.getRoomList();
            roomList.get(RANDOM.nextInt(roomList.size())).setStatusRoom(StatusRoom.REPAIR);
        }


        // +4. Изменить цену номера или услугу.   (реализовано ранее):
        {
            // изменим цену номера
            List<Map.Entry<NumberBeds, Integer>> roomPriceList = new ArrayList(hotel.getRoomPrice().entrySet());   // конвертировали прайс в List
            Map.Entry<NumberBeds, Integer> numberBedsIntegerEntry = roomPriceList.get(RANDOM.nextInt(roomPriceList.size()));   // получим случайное значение (наименование)
            NumberBeds numberBeds = numberBedsIntegerEntry.getKey();   // получили ключь
            int roomPrice = 123;   // устанавливаем новое значение.
            hotel.getRoomPrice().put(numberBeds, roomPrice);   // меняем прайс.

            // изменим цену услуги
            List<String> servicePriceNamesList = new ArrayList(hotel.getServicePrice().keySet());   // конвертировали прайс в List
            String servicePriceName = servicePriceNamesList.get(RANDOM.nextInt(servicePriceNamesList.size()));   // получим случайное значение (наименование)
            int servicePrice = 321;   // устанавливаем новое значение.
            hotel.getServicePrice().put(servicePriceName, servicePrice);
        }


        // +5. Добавить номер или услугу.   (реализовано ранее):
        {
            // добавим новый номер
            int number = 112233;
            NumberBeds numberBeds = NumberBeds.DOUBLE;
            StarsRoom stars = StarsRoom.FIVE;
            Room room = new Room(number, numberBeds, stars);
            hotel.getRoomList().add(room);

            // добавим новую услугу
            String newService = "newService!";
            int priceService = 223344;
            hotel.getServicePrice().put(newService, priceService);
        }

    }

    private static StringBuilder lodgingGuestsListToStringBuilder(List<LodgingGuest> lodgingGuestsList, ProvidedServiceComparator providedServiceComparator) {
        StringBuilder result = new StringBuilder();
        for (LodgingGuest lodgingGuest: lodgingGuestsList) {   // будем итерироваться по учеткам всех гостей отеля/ переберем
            List<ProvidedService> providedServices = lodgingGuest.getProvidedServices();
            providedServices.sort(providedServiceComparator);
            result.append("Имя постояльца: " + lodgingGuest.getGuest().getName() + " , перечень услуг:\n");
            for (ProvidedService providedService: providedServices) {
                result.append(" service: " + providedService.getNameService());
                result.append(" \t price: " + providedService.getPriceService());
                result.append(" \t date: " + providedService.getDate() + "\n");
            }
        }
        return result;
    }

    private static StringBuilder servicePriceListToStringBuilder(List<Map.Entry<String, Integer>> servicePriceList) {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, Integer> entry: servicePriceList) {
            String key = entry.getKey();
            builder.append(" service: " + key);
            Integer value = entry.getValue();
            builder.append(" \t price: " + value + "\n");
        }
        return builder;
    }

    private static void roomListPrinter(String message, List<Room> roomList) {
        StringBuilder result = new StringBuilder(DELIMITER + "\n" + message + "\n");
        for (Room room: roomList) {
            result.append(room + "\n");
        }
        System.out.println(result);
    }
    private static void lodgingGuestsListPrinter(String message, List<LodgingGuest> lodgingGuests) {
        StringBuilder result = new StringBuilder(DELIMITER + "\n" + message + "\n");
        for (LodgingGuest lodgingGuest: lodgingGuests) {
            result.append(lodgingGuest + "\n");
        }
        System.out.println(result);
    }

}
